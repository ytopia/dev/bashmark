# Bashmark
## Install

```bash
curl https://gitlab.com/youtopia.earth/dev/bashmark/raw/master/install | bash
```

## Description
Bashmark is bookmark utility for bash provided with tab completion support.

## Shell Commands
```bash
s <bookmark_name> - Saves the current directory as "bookmark_name"
g <bookmark_name> - Goes (cd) to the directory associated with "bookmark_name"
p <bookmark_name> - Prints the directory associated with "bookmark_name"
d <bookmark_name> - Deletes the bookmark
b                 - Lists all available bookmarks
```

## Example Usage
```bash
$ cd /var/www/
$ s webfolder
$ cd /usr/local/lib/
$ s locallib
$ b
$ g web<tab>
$ g webfolder
```

## Where Bashmarks are stored

All of your directory bookmarks are saved in a file called ".sdirs" in your HOME directory.

## About
based on https://github.com/huyng/bashmarks

## License
[MIT](https://choosealicense.com/licenses/mit/)